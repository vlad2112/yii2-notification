<?php

use basyan\notification\BitrixNotifier;
use Faker\Generator;

class BitrixNotifierTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /** @var Generator */
    protected $faker;
    /** @var BitrixNotifier */
    protected $notifier;

    protected function _before()
    {
        $this->notifier = Yii::$app->bitrix;
        $this->faker = Faker\Factory::create();
    }

    protected function _after()
    {
    }

    // tests
    public function testSendMessage()
    {
        $this->notifier->message->text = $this->faker->text;
        $this->tester->assertTrue($this->notifier->message->send());
    }

    // tests
    public function testHasErrors()
    {
        $this->notifier->chatId = uniqid();
        $message = new \basyan\notification\Message($this->notifier);
        $message->text = $this->faker->text;
        $message->send();
        $this->tester->assertTrue($message->hasErrors());
    }
}