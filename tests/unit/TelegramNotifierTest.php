<?php

use basyan\notification\TelegramNotifier;
use Faker\Generator;

class TelegramNotifierTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /** @var Generator */
    protected $faker;
    /** @var TelegramNotifier */
    protected $notifier;

    protected function _before()
    {
        $this->notifier = Yii::$app->telegram;
        $this->faker = Faker\Factory::create();
    }

    protected function _after()
    {
    }

    // tests
    public function testSendMessage()
    {
        $this->notifier->message->text = $this->faker->text;
        $this->tester->assertTrue($this->notifier->message->send());
    }

    // tests
    public function testHasErrors()
    {
        $this->notifier->token = uniqid();
        $message = new \basyan\notification\Message($this->notifier);
        $message->text = $this->faker->text;
        $message->send();
        $this->tester->assertTrue($message->hasErrors());
    }
}